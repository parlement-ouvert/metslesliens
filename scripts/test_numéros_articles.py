#!/usr/bin/python3

"""
./scripts/test_numéros_articles.py fichier_articles

Le fichier_article doit contenir des noms d’articles à raison de un par ligne.

Pour tester sur l’ensemble des noms d’articles de la base LEGI, il est
possible d’utiliser https://github.com/Legilibre/legi.py avec une des requêtes :

  SELECT DISTINCT num FROM articles ORDER BY num;
  SELECT num, COUNT(num) AS cnum FROM articles GROUP BY num ORDER BY cnum;

Cela donne environ 140 000 noms d’articles (en mars 2018). La 2e version ajoute
le nombre d’occurences par nom d’article. Un tel export est donné sur
https://framagit.org/parlement-ouvert/metslesliens/snippets, il peut
être ancien. La performance est actuellement d’environ 73 µs par nom.

Voir aussi le fichier docs/légistique.md.
"""

import time, re, sys, os.path
import parsimonious

repertoire = os.path.realpath( os.path.dirname( __file__ ) )

if len( sys.argv ) == 2:
    fichier_numeros_article = sys.argv[1]
else:
    raise Exception()

f = open( os.path.join( repertoire, '..', 'metslesliens', 'grammaire-liens.txt' ), 'r' )
grammaire = f.read()
f.close()

grammaire = "regle = numero_article / nom_article\n" + grammaire
grammaire = parsimonious.Grammar( grammaire )

f = open( fichier_numeros_article, 'r' )
numeros_article = f.read().splitlines()
f.close()

regex_nb_occurrences = re.compile( '\|([0-9]+)$' )
for i in range( 0, len( numeros_article ) ):
    occurrences = re.search( regex_nb_occurrences, numeros_article[i] )
    if occurrences:
        numeros_article[i] = re.sub( regex_nb_occurrences, '', numeros_article[i] )
        numeros_article[i] = ( numeros_article[i], int( occurrences.group(1) ) )
    else:
        numeros_article[i] = ( numeros_article[i], '' )

# Les pré- et post-traitements sont en-dehors de cette boucle pour éviter de trop fausser le calcul du temps
resultats = []
t = time.perf_counter()
for numero_article in numeros_article:
    try:
        grammaire.parse( numero_article[0] )
        resultats.append( True )
    except:
        resultats.append( False )
temps = time.perf_counter() - t

nb_succes = 0
volume_total = 0
volume_reconnu = 0
for i in range( 0, len( numeros_article ) ):
    if resultats[i]:
        chaine = 'succès'
        nb_succes += 1
        if isinstance( numeros_article[i][1], int ):
            volume_reconnu += numeros_article[i][1]
    else:
        chaine = 'échec '
    if numeros_article[i][1] == '':
        print( chaine + '"' + numeros_article[i][0] + '"' )
    else:
        volume_total += numeros_article[i][1]
        print( chaine + '"' + numeros_article[i][0] + '" ' + str( numeros_article[i][1] ) )

print( '-------------------------------------------' )
print( 'Non-reconnus = ' + str( len( numeros_article ) - nb_succes ) + ' / ' + str( len( numeros_article ) ) )
print( 'Noms reconnus  = ' + str( nb_succes / len( numeros_article ) * 100 ) + ' %' )
if volume_total:
    print( 'Volume reconnu = ' + str( volume_reconnu / volume_total * 100 ) + ' %' )
print( 'Temps d’exécution = ' + str( temps / len( numeros_article ) ) + ' s / article' )

# vim: set ts=4 sw=4 sts=4 et:
