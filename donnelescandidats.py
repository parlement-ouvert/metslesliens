#!/usr/bin/env python3

# -*- coding: utf-8 -*-

import sys
import metslesliens

# Paramètres
options = [arg for arg in sys.argv[1:] if arg.startswith( '--' )]
format = 'texte'
if len( options ) == 1:
    format = options[0].split( '=' )[1]
    if format not in ( 'texte', 'arbre', 'structuré', 'structuré-index', 'debug' ):
        raise Exception( 'Le format donné est invalide.' )

args = [arg for arg in sys.argv[1:] if not arg.startswith( '--' )]
if len(args) != 1:
    raise Exception( 'Le nom du fichier doit être donné en argument.' )
nomfichier = args[0]

try:
    with open( nomfichier, 'r' ) as f: texte = f.read()
except FileNotFoundError:
    texte = nomfichier

# Appel à la librairie
candidats = metslesliens.donnelescandidats( texte, format, [ 'Conseil constitutionnel' ] )


# Résultats
if format == 'arbre':
    for candidat in candidats:
        print( 'index: ' + str( ( candidat.start, candidat.end ) ) )
        print( 'ligne: ' + str( texte.count( '\n', 0, candidat.start )+1 ) )
        print( candidat )
        print( '------------------------------------' )
elif format == 'structuré' or format == 'structuré-index':
    for candidat in candidats:
        print( 'index: ' + str( candidat['index'] ) )
        print( 'ligne: ' + str( texte.count( '\n', 0, candidat['index'][0] )+1 ) )
        print( 'expr:  ' + texte[candidat['index'][0]:candidat['index'][1]] )
        for i in reversed(candidat):
            if i != 'index':
                print( i + ': ' + str( ( candidat[i], ) )[1:-2] )
        if len( candidat ) == 1:
            print( '(données structurées manquantes)' )
        print( '------------------------------------' )
elif format == 'texte' and len( candidats ):
    lenindex = max( len( str( candidats[ len(candidats)-1 ][0] ) ), len( 'index' ) )
    lenligne = max( len( str( texte.count( '\n', 0, candidats[ len(candidats)-1 ][0] )+1 ) ), len( 'ligne' ) )
    affichage = '%%%ds %%%ds %%s' % ( lenindex, lenligne )
    print( affichage % ( 'index', 'ligne', 'texte' ) )
    for candidat in candidats:
        print( affichage % ( candidat[0], texte.count( '\n', 0, candidat[0] )+1, candidat[1] ) )
elif format == 'debug' and len( candidats ):
    lenindex = max( len( str( candidats[ len(candidats)-1 ][0] ) ), len( 'index' ) )
    lenligne = max( len( str( texte.count( '\n', 0, candidats[ len(candidats)-1 ][0] )+1 ) ), len( 'ligne' ) )
    lenavant = max( max( [ len( candidat[1] ) for candidat in candidats ] ), len( 'avant ' ) )
    affichage = '%%%ds %%%ds %%%ds%%s' % ( lenindex, lenligne, lenavant )
    print( affichage % ( 'index', 'ligne', 'avant ', 'texte' ) )
    for candidat in candidats:
        print( affichage % ( candidat[0], texte.count( '\n', 0, candidat[0] )+1, candidat[1], candidat[2] ) )

print( '> %d candidat%s à la ligature' % ( len( candidats ), '' if len( candidats ) == 1 else 's' ) )

# vim: set ts=4 sw=4 sts=4 et:
