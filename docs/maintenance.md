Maintenance du code et de la grammaire
======================================

Maintenance de la grammaire
---------------------------

La grammaire, via Parsimonious, est progressive (*forward*) et gloutonne (*greedy*). Les conseils ci-après détaillent la rédaction de la grammaire.

* **Alternatives :** La grammaire « entree = "a" / "aa" » sur la chaîne "aa" capture "a" et renvoie l’exception parsimonious.exceptions.IncompleteParseError : la première occurence de l’alternative est capturée, la règle entière est donc capturée et on passe à la suite. Pour obtenir l’effet escompté, i.e. laisser une chance à l’expression "aa" d’être capturée, il faut rédiger la grammaire en mettant la plus longue expression en premier : la grammaire doit être « entree = "aa" / "a" ». Ce conseil de rédaction vaut aussi pour les expressions régulières, raison pour laquelle il faut ordonner la règle nomscodes par ordre décroissant de longueur de chaîne.
* **Aspect glouton+progressif :** Avec la grammaire « entree = ( ","? "a" )+ ",a" », la chaîne "a,a,a" renvoie l’exception « parsimonious.exceptions.ParseError: Rule 'entree' didn't match at 'a,a,a' ». La logique interne est de capturer le plus possible de fois la chaîne interne aux parenthèses, donc il n’y a plus de caractères une fois cette opération faite ; il n’y a pas de retour en arrière possible pour tenter de capturer l’expression. Pour contourner ceci, il faut faire en sorte d’avoir une disjonction stricte entre une chaîne répétée et la suite de l’expression.
* **Aspect glouton+progressif, niveau avancé (vécu) :** prenons la grammaire « entree = "alinéas" ~" +" ~"premier" ( ~r",? \*et +|, \*" ~r"deuxième|troisième" )+ !~"[a-z]" », elle capture bien l’expression "alinéas premier, deuxième et troisième" avec `grammaire.match( "premièrement aux alinéas premier, deuxième et troisième. Suite`, 17 )`, mais elle ne la capture pas avec `grammaire.match( "premièrement aux alinéas premier, deuxième et troisième, et deuxièmement", 17 )` (qui retourne l’exception `parsimonious.exceptions.ParseError: Rule \<Regex ~",? \*et +|, \*"u\> didn't match at 'ment'`). On pourra alors modifier la grammaire de la façon suivante « entree = "alinéas" ~" +" ~"premier" ( ~r",? \*et +|, \*" ~r"deuxième|troisième" !~"[a-z]" )+ » qui capturera correctement l’expression avec `grammaire.match( "premièrement aux alinéas premier, deuxième et troisième, et deuxièmement", 17 )`.
* **Performance :** avec la version 0.8.0 de Parsimonious (ça s’améliorera peut-être), les expressions régulières sont bien plus performantes que les expressions natives de Parsimonious. Ainsi, si on a une règle « lettres_enumeration = "a" / "b" / "c" / "d" / ... / "z" » et qu’on prend une suite aléatoire de 300 000 lettres de l’alphabet, la grammaire « lettres_enumeration_plusieurs = lettres_enumeration\* » capture le texte en 28,1 secondes, la grammaire « lettres_expr_reg_plusieurs = ~"[a-z]"\* » en 3,3 secondes (soit un facteur 10), et la grammaire « lettres_expr_reg_etoile = ~"[a-z]\*" » en 0,0035 seconde (soit un facteur 1000 avec la précédente, 10 000 avec la première). Ceci doit être gardé à l’esprit mais doit être contrebalancé avec les bénéfices des expressions natives, notamment la granularité et la modularité qu’elles autorisent.
* **Facilité des réutilisations :** afin de faciliter la lecture des arbres résultats, on pourra ajouter des parenthèses bien placées afin d’obtenir des hiérarchies comparables dans des expressions comparables, par exemple dans les règles « lienarticle = "article" espace designationarticle » et « lienarticles = "articles" espace designationarticle ( ( enumeration / plage ) ( ~"précédents"i / ~"suivants"i / designationarticle ) )+ », on peut vouloir faire en sorte que le 3e élément de la règle soit l’ensemble des numéros d’articles capturés, on peut alors rédiger la seconde règle comme « lienarticles = "articles" espace ( designationarticle ( ( enumeration / plage ) ( ~"précédents"i / ~"suivants"i / designationarticle ) )+ ) ».


Maintenance des codes de loi dans la grammaire
----------------------------------------------

La liste des codes dans la grammaire nécessite d’être mise à jour lorsque de nouveaux codes sont créés.

Pour cela :
* extraire la liste exhaustive des codes, par exemple la requête SQL avec la base de données [legi.py](https://github.com/Legilibre/legi.py) est :
  `SELECT titrefull FROM textes_versions WHERE nature = 'CODE';`
* concaténer les valeurs avec comme séparateur `" / "` et bien sûr ajouter un `"` en début et fin des valeurs pour que chaque nom de texte soit proprement entouré de guillemets et que ces noms soient séparés par une barre oblique (*slash*) ;
* retirer les quelques codes ayant comme suffixe `(nouveau)` ou `(ancien)` (qui sont gérés par une règle de niveau supérieur) **tout en laissant la forme basique**, par exemple il s’agira de retirer `Code forestier (nouveau)` (`Code forestier` existe et suffit) ainsi que `Code rural (nouveau)` et `Code rural (ancien)`, mais comme `Code rural` n’existe pas en tant que tel il faut l’ajouter
* ajouter les codes qui ne sont que suffixés par un numéro de version comme `(édition 2004)`
* trier les codes par longueur (du nom du code) décroissante
* faire les transformations suivantes :
  " " → " +"
  "'" → "['’] *"
  "’" → "['’] *"
  "(" → "\(" (un anti-slash + parenthèse)
  ")" → "\)" (un anti-slash + parenthèse)
* il est possible de compacter un peu certaines variantes de codes avec des parenthèses-regex (au sens d’alternatives donc) 
  * pour les codes des marchés publics et ses différentes variantes par années,
  * pour les codes des douanes et forestier de Mayotte (par ex. "des +douanes( +de +Mayotte)?")
  * pour le CGI, il faut rendre la 2e partie optionnelle : "général +des +impôts(, +CGI)?"
  * pour le code de commerce, l’expression « code du commerce » apparaît, on peut assouplir la contrainte avec "d[eu] +commerce"
* concaténer les codes en les séparant par des barres verticales "|"
* cela donne la regex à mettre entre parenthèses

À l’heure de l’écriture de cette documentation, les codes à retirer sont :
* Code forestier (nouveau)
* Code de justice militaire (nouveau)
* Code minier (nouveau)
* Code rural (nouveau)
* Code pénal (ancien)
* Code de la route (ancien)
* Code de commerce (ancien)
* Code rural (ancien)
Et il faut ajouter :
* Code rural
* Code des marchés publics
* Code général des impôts
